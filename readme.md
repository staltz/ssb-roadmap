# SSB Roadmap

> A roadmap diagram detailing the what needs to be done next, to advance some SSB projects

**Feel free to edit the graph below with [Mermaid syntax](https://mermaidjs.github.io/).**

```mermaid
graph LR
    %%%%%%%%%%%%%%%%%%%%%%
    %% TASK DEFINITIONS %%
    %%%%%%%%%%%%%%%%%%%%%%

    protocol-docs %% https://ssbc.github.io/scuttlebutt-protocol-guide/
    docs-website-aggregation %% website renewal and centralization
    gossip-refactor
    ssb-conn-db
    ssb-conn-hub
    ssb-conn-utils
    text-as-blob
    ssb-legacy-conn
    ssb-friends3
    same-as
    muxrpc-push-stream %% maybe @dominic is doing this
    soft-edit-msg
    wiki-msg
    account-milestones
    offlog-online-authenticated-chat
    recover-id-from-words

    subgraph performance
        rust-ssb %% sunrise choir is doing this
        ssb-binary-database %% maybe @dominic is doing this
        pull-drain-gently
        gently-build-flume-indexes
        lazy-indexes
        holding-tank-log
        replicate-close-friends-first
    end

    subgraph servers
        ssb-tunnel
        cjdns-nodejs-bindings
        multiserver-cjdns
        rooms
        ssb-dht-invites
        broadcast-pubs
        hyperswarm-usage
        dht-invites-in-production
        ssb-bluetooth %% https://github.com/Happy0/ssb-bluetooth/
    end

    subgraph free listening
       private-block
       private-block-in-production
       auditable-user-invites
       ssb-lists
       ssb-lists-in-production
    end

    subgraph trust and safety
        blame-a-blob
        byte-count-per-feed
        storage-usage-stats
        hop-2-usernames
        publicWebHosting
        publicWebHosting-config-in-apps
        flag-a-msg
        delete-blob
        block-blob
        blob-management
        blob-garbage-collection
        delete-blobs-of-user
        delete-feed-from-flume
        soft-delete-msg
        private-groups
    end

    %%%%%%%%%%%%%%%%%%%%%%%%
    %% STATUS DEFINITIONS %%
    %%%%%%%%%%%%%%%%%%%%%%%%
    %% To do:
    class text-as-blob,ssb-lists-in-production,flag-a-msg,blame-a-blob,byte-count-per-feed,storage-usage-stats,hop-2-usernames,publicWebHosting-config-in-apps,blob-garbage-collection,blob-management,block-blob,delete-blobs-of-user,soft-edit-msg,soft-delete-msg,wiki-msg,gently-build-flume-indexes,lazy-indexes,replicate-close-friends-first,account-milestones,offlog-online-authenticated-chat,docs-website-aggregation,cjdns-nodejs-bindings,multiserver-cjdns,holding-tank-log,broadcast-pubs todo

    %% Work in progress:
    class rust-ssb,same-as,delete-feed-from-flume,muxrpc-push-stream,private-groups,auditable-user-invites,ssb-binary-database,hyperswarm-usage,dht-invites-in-production wip

    %% Done:
    class protocol-docs,ssb-friends3,recover-id-from-words,pull-drain-gently,rooms,ssb-conn-utils,private-block-in-production,ssb-tunnel,private-block,gossip-refactor,ssb-lists,publicWebHosting,delete-blob,ssb-dht-invites,ssb-conn-db,ssb-bluetooth,ssb-legacy-conn,ssb-conn-hub done

    %%%%%%%%%%%%%%%%%%%%%%%
    %% ORDER CONSTRAINTS %%
    %%%%%%%%%%%%%%%%%%%%%%%

    protocol-docs --> rust-ssb
    protocol-docs --> docs-website-aggregation
    ssb-tunnel --> rooms
    ssb-friends3 --> same-as
    same-as --> account-milestones
    ssb-lists --> ssb-lists-in-production
    private-block --> private-block-in-production
    blame-a-blob --> byte-count-per-feed
    byte-count-per-feed --> storage-usage-stats
    publicWebHosting --> publicWebHosting-config-in-apps
    delete-blob --> blob-management
    block-blob --> blob-management
    blob-garbage-collection --> blob-management
    blame-a-blob --> delete-blobs-of-user
    delete-blobs-of-user --> delete-feed-from-flume
    soft-edit-msg --> soft-delete-msg
    soft-edit-msg --> wiki-msg
    pull-drain-gently --> gently-build-flume-indexes
    gently-build-flume-indexes --> replicate-close-friends-first
    rooms --> offlog-online-authenticated-chat
    cjdns-nodejs-bindings --> multiserver-cjdns
    ssb-dht-invites --> dht-invites-in-production
    hyperswarm-usage --> dht-invites-in-production
    holding-tank-log --> gently-build-flume-indexes
    ssb-conn-db --> ssb-legacy-conn
    ssb-conn-hub --> ssb-legacy-conn
    ssb-conn-utils --> ssb-legacy-conn
    ssb-legacy-conn --> gossip-refactor
    gossip-refactor --> rooms
    gossip-refactor --> broadcast-pubs

    %%%%%%%%%%%%
    %% Styles %%
    %%%%%%%%%%%%
    classDef cluster opacity:0.25;
    classDef todo fill:#fbb,stroke-width:0px;
    classDef wip fill:#ee7,stroke-width:0px;
    classDef done fill:#7e7,stroke-width:0px;
```

<a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/"><img alt="Creative Commons Licence" style="border-width:0" src="https://i.creativecommons.org/l/by-sa/4.0/88x31.png" /></a><br />This work is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/">Creative Commons Attribution-ShareAlike 4.0 International License</a>.
